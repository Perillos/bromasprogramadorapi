
//Varialbes
/* Globales */
const pokeAPIurl = 'https://pokeapi.co/api/v2/pokemon/?offset=0&limit=151'
const allIndividualPokemos = []
const allNameTypesPokemos = ['all']


/* Elementos del HTML */
const pokedex$$ = document.getElementById("pokedex");


// Funciones

/* Pedimos todos los Pokemos */
const getAllPokemons = () => {
    return fetch(pokeAPIurl)
    .then(respon => respon.json())
    .then(respon => respon)
    .catch(error => console.log('Error al traer todos los Pokemons', error))
}
/* Iteración sobre la url de result, petición de cada pokemon, meterlo en array de allIndividualPokemos */
const getPokemon = async (pokemons) => {
    for (const pokemon of pokemons) {
        const getIndividuallPokemons = (pokemourl) => {
            return fetch (pokemourl)
            .then(respon => respon.json())
            .then(respon => allIndividualPokemos.push(respon))
            .catch(error => console.log('Error al traer un Pokemon', error))
        }
        await getIndividuallPokemons(pokemon.url)
    }
}

/* Seleccionar el pokemon presinado */
const pressPokemon = (e) => {
    if(e.target.classList.contains('card-title')) {
        const pokeSelec = allIndividualPokemos.find(pokemon => e.target.value === pokemon.name)
        drawIndividualPokemon(pokeSelec)
    }
    if(e.target.classList.contains('back')) {
        welcomePokedex()
    }
}
/* Filtrado por tipo */
const filterByType = (event) => {
    const typeToFilter = event.target.id;
    //  Se muestran
    if (typeToFilter === "all") {
      return drawPokedex(allIndividualPokemos);
    }
    // Filtrado por tipos
    const filtered = allIndividualPokemos.filter((pokemon) => {
        const pokeTypes = pokemon.types;
        const pokeTypePoke =[]
        pokeTypes.forEach(pokeType => {
            pokeTypePoke.push(pokeType.type.name)
        });
        if (pokeTypePoke.includes(typeToFilter)) {
            return pokemon;
        }
    });
    
    drawPokedex(filtered);
};

/* Pinta el pokemon individula */
const drawIndividualPokemon = (pokemonSelector) => {
    pokedex$$.innerHTML = ''
    const pokeSelector = pokemonSelector
    const urlPokeType = pokeSelector
    
    const pokeLi = document.createElement('li')
    pokeLi.classList.add("card")
    const pokeTypes = pokeSelector.types
    const htmlunic = `
        <img src=${pokeSelector.sprites.versions['generation-v']['black-white'].animated['front_default']} alt=${pokeSelector.name}>
        <p class="card-title">${pokeSelector.name}</p>
        <button class="back">BACK</button>
    `
    /* Añadiendo al DOM */
    pokeLi.innerHTML = htmlunic
    const typesDiv = document.createElement('div')
    for (const pokeType of pokeTypes) {
        const typeDiv = document.createElement('div')
        const typeButton = document.createElement('Button')
        typeButton.innerText = pokeType.type.name
        typeDiv.innerText = ('Type: ')
        // typeA.classList.add('types')
        typeButton.id = pokeType.type.name
        typeDiv.classList.add(pokeType.type.name)
        typeDiv.appendChild(typeButton)
        typesDiv.appendChild(typeDiv)
        drawTypes(pokeType)
        typeButton.addEventListener('click', filterByType)
    }

    pokeLi.appendChild(typesDiv)
    pokedex$$.appendChild(pokeLi)
    pokeLi.addEventListener('click', pressPokemon)
}

/* Buscar y pintar contra que es debil y fuerte el pokemon */
const drawTypes = async (allPokeType) => {
    const urlPokeIndivudualType = allPokeType.type.url
    const typePokeIndivudualType = allPokeType.type.name
    const getTypes = (pokeIndividualUrlType) => {
        return fetch(pokeIndividualUrlType)
        .then(respon => respon.json())
        .then(respon => respon)
        .catch(error => console.log('Error al traer el Tipo', error))
    }

    await getTypes(urlPokeIndivudualType)
    const typeIndivudualPokemon = await getTypes(urlPokeIndivudualType)
    const damagesFrom = typeIndivudualPokemon['damage_relations']['double_damage_from']
    let damageP = document.querySelector(`.${typePokeIndivudualType}`)
    const damageFromDiv = document.createElement('div')
    damageFromDiv.innerText = 'Weak against:'
    for (const damageFrom of damagesFrom) {
        const fromLi = document.createElement('button')
        fromLi.innerText = damageFrom.name
        fromLi.id = damageFrom.name;
        damageFromDiv.appendChild(fromLi)
        fromLi.addEventListener('click', filterByType)
    }
    damageP.appendChild(damageFromDiv)
    const damagesTo = typeIndivudualPokemon['damage_relations']['double_damage_to']
    const damageToDiv = document.createElement('div')
    damageToDiv.innerText = 'Strong against:'
    for (const damageto of damagesTo) {
        const fromLi = document.createElement('button')
        fromLi.innerText = damageto.name
        fromLi.id = damageto.name;
        damageToDiv.appendChild(fromLi)
        fromLi.addEventListener('click', filterByType)
    }
    damageP.appendChild(damageToDiv)
    
}
// function drawPokedex () {} /* Si le voy a dar un nombre a la función no es mas rapido el metodo tradicional? */

// Dibuja todos los pokemons
const drawPokedex = (pokemonsDraw) => {
    pokedex$$.innerHTML = ''
    pokemonsDraw.forEach(pokeInfo => {
        /* Creando elementos del DOM */
        const pokeLi = document.createElement('li')
        /* Incluyendo clases */
        pokeLi.classList.add("card")
        pokeLi.id = pokeInfo.name
        /* Creando cada Pokemon, imagen y nombre */
        const html = `
            <img src=${pokeInfo.sprites.versions['generation-v']['black-white'].animated['front_default']} alt=${pokeInfo.name}>
            <button class="card-title" value="${pokeInfo.name}">${pokeInfo.name}</button>
        `
        /* Añadiendo al DOM */
        pokeLi.innerHTML = html
        pokedex$$.appendChild(pokeLi)
        pokeLi.addEventListener('click', pressPokemon)
    });
}

/* Filtro por nombre y número */
const filter = (event) => {
    const inputValue = event.target.value.toLowerCase();
    const filtered = allIndividualPokemos.filter((pokemon) => {
      const matchName = pokemon.name.toLowerCase().includes(inputValue);
      const matchId = pokemon.id === Number(inputValue);
      return matchName || matchId;
    });
    drawPokedex(filtered);
};
const addAllMyEventsListeners = () => {
    document.getElementById("input-search").addEventListener("input", filter);
};

/* Pintar botones de tipos */
const drawTypesButtons = () => {
    allIndividualPokemos.forEach((individualPoke) => {
        const individualTypePoke = individualPoke.types
        individualTypePoke.forEach((individualType) =>{
            const nameType = individualType.type.name
            if (!allNameTypesPokemos.includes(nameType)) {
                allNameTypesPokemos.push(nameType)
            }
        })
    })
    const typesContainer$$ = document.querySelector(".types");
    allNameTypesPokemos.forEach((type) => {
        const span = document.createElement("span");
        span.id = type;
        span.addEventListener("click", filterByType);
        span.innerText = type;
        typesContainer$$.appendChild(span);
      });
};

{
    // const typesContainer$$ = document.querySelector(".types");
    // allIndividualPokemos.forEach((individualPoke) => {
    //     const pokeTypes = individualPoke.types
    //     pokeTypes.forEach((individualTypePoke) => {
    //         console.log(individualTypePoke.type.name);
    //         let span = document.createElement("span");
    //         // span.classList.add(type);
    //         // span.addEventListener("click", filterByType);
    //         // span.innerText = individualTypePoke.type.name;


    //     })
        // typesContainer$$.appendChild(span);


}


/* Función de inicio */
const welcomePokedex = async () => {
    addAllMyEventsListeners()
    const allPokemons = await getAllPokemons() // Todos los pokemos
    const idividualPokemons = await getPokemon(allPokemons.results) // Cojo el result de todos los pokemons
    const drawInPokedex = drawPokedex(allIndividualPokemos)
    drawTypesButtons();
}


/* Incio la función */
welcomePokedex()
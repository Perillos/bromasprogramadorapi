const getBobs = () => {
    return fetch('https://bobsburgers-api.herokuapp.com/characters/')// Se conecta con la API
    .then (response => response.json()) // Me da respuesta de la conexión
    .then(burgers => burgers) // Me da el contenido de la API
    .catch(error => console.log(error)) // Si hay un error en el proceso me lo indica
}


const pintImag = (burgers) => {
    burgers.forEach(ele => {
        let imag = document.createElement('img')
        imag.src = ele.image
        document.body.appendChild(imag)
        
    });

}





const init = async () => {
    const burger = await getBobs()
    pintImag(burger)

}




init()
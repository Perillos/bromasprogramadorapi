const getMamApi = () => {
    return fetch('https://v2.jokeapi.dev/joke/Any?lang=es')
    .then(response => response.json())
    .then(joke => joke)
    /* Por que aqui pone dos parentesis y en el then solo uno? */
    .catch(error => console.log(error))
}

const jokeInHtml = (joke) => {
    if (joke.type == 'single') {
        document.querySelector('.setup').innerText = joke.joke;
        document.querySelector('.delivery').innerText = '';
    }else{
        document.querySelector('.setup').innerText = joke.setup;
        document.querySelector('.delivery').innerText = joke.delivery;
    }
}


const changeJoke = async () => {
    const joke = await getMamApi();
    jokeInHtml(joke);
}

const changeMyJoke = () => {
    const buttonChange = document.querySelector('#change-joke');
    buttonChange.addEventListener('click', changeJoke);

}



const comeMyWeb = async () => {
    changeMyJoke();
    
    const joke = await getMamApi();
    jokeInHtml(joke);

}


/* Cuando entre a mi web se inicia este evento */
window.onload = comeMyWeb;